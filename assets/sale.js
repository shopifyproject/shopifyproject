class FinalSale extends HTMLElement {
  constructor() {
    super();

    const cartNotification = new CartNotification(),
        sectionId = 'template--16834150662454__5bf5f424-5760-475b-8902-708ab6cef2b7',
        sectionToAppend = $('#MainContent'),
        addToCart = $('[value="Add to cart"]'),
        cart = $('cart-notification') || $('cart-drawer');

    function getProductByOptions(el) {
      let productConfig = []
      $(el).closest('.custom-product-options').find('input.checked').each((id, elem) => {
        productConfig.push($(elem).val())
      })
      return productConfig.join(' / ')
    }

    function getProductId(element, config) {
      let productId = '',
          current = '';

      $(element).closest('.custom-product-options').find('.product-variant').each((id, item) => {
        let currentVariant = $(item).text().split('-')[0].trim()
        if(currentVariant === config) {
          productId = $(item).attr('value');
          current = currentVariant;
        }
      })

      return [productId, current]
    }



    /*
     Adding to cart checked product variant
    */
    addToCart.on('click', function (event) {
      event.preventDefault();
      const form = this.parentNode.closest('form'),
          formData = new FormData(form);

      if (cart) {
        formData.append('sections', cartNotification.getSectionsToRender().map(section => section.id));
        formData.append('sections_url', window.location.pathname);
        cartNotification.setActiveElement(document.activeElement);
      }

      fetch('/cart/add.js', {
        method: 'POST',
        body: formData
      })
          .then(response => response.json())
          .then(response => {
            cartNotification.renderContents(response);
          })
          .catch((error) => {
            console.error('Error:', error);
          });
    })

    /*
      Changing price when some variant checked
     */
    $(document).on('click', '[data-click="variants"]', function () {
      var handle = $(this).data('handle'),
          currentVariant,
          priceField = $(this).closest('.card').find('.price'),
          addToCartButton = $(this).closest('.card').find('[value="Add to cart"]'),
          idInput = addToCartButton.closest('form').find('input[name="id"]'),
          variantBadges = $(this).closest('.product-form__input').find('[data-click="variants"]');

      variantBadges.removeClass('checked');
      $(this).addClass('checked');
      let id = getProductId(this, getProductByOptions(this))[0];
      idInput.val(id);
      currentVariant = getProductId(this, getProductByOptions(this))[1]


      $.getJSON(window.Shopify.routes.root + `products/${handle}.js`, function (product) {
        if (product.variants) {
          product.variants.forEach(variant => {
            if (variant.title === currentVariant) {
              fetch(`/products/${handle}?variant=${variant.id}`)
                  .then((response) => response.text())
                  .then((responseText) => {
                    const html = new DOMParser().parseFromString(responseText, 'text/html'),
                          price = html.querySelector(`.price`);
                    priceField.html(price.innerHTML);
                  })
                  .catch(error => console.error(error))
              ;
            }
          })
        }
      });
    });

    /*
     Add sections by fetching sections IDs
    */
    if (window.location.pathname === '/pages/final-sale') {
      function handleResponse() {
        let html = JSON.parse(this.responseText);
        sectionToAppend.append(html['template--16834150662454__6ff6e04b-15e0-4a0d-ad55-69734f13a8d2'])
      }

      const request = new XMLHttpRequest();
      request.addEventListener('load', handleResponse);
      request.open('GET', '/?sections=template--16834150662454__6ff6e04b-15e0-4a0d-ad55-69734f13a8d2,footer', true);
      request.send();


      fetch(window.location.pathname + `/?sections=${sectionId}`)
          .then(res => res.json())
          .then(res => {
            let html = res[sectionId]
            sectionToAppend.append(html)
          })
    }
  }
}

customElements.define('final-sale', FinalSale);

new FinalSale();