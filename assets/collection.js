async function loadMoreProducts() {
const products_on_page = document.getElementById('product-grid');
let next_url = products_on_page.dataset.nextUrl;

const load_more_btn = document.getElementsByClassName('load-more_btn')[0];
const load_more_spinner = document.getElementsByClassName('load-more_spinner')[0];
async function getNextPage() {
  try {
    let res = await fetch(next_url);
    return await res.text();
  } catch (error) {
    console.log(error);
  }
}

  load_more_btn.style.display = 'none';
  load_more_spinner.style.display = 'block';
  let nextPage = await getNextPage();

  const parser = new DOMParser();
  const nextPageDoc = parser.parseFromString(nextPage, 'text/html');

  load_more_spinner.style.display = 'none';

  const productgrid = nextPageDoc.getElementById('product-grid');
  const new_products = productgrid.getElementsByClassName('grid__item');
  const new_url = productgrid.dataset.nextUrl;
  if (new_url) {
    load_more_btn.style.display = 'block';
  }
  next_url = new_url;
  for (let i = 0; i < new_products.length; i++) {
    products_on_page.appendChild(new_products[i]);
  }
}


(function ($) {

  $(document).ready(function () {
    if ($('.vintage-filter-box').length) {

      $(document).on('click', '.custom-select__name', function() {
        $(this).closest('.custom-select').toggleClass('m-sl-open');
        $(this).next().slideToggle();

        $('.custom-select__item').each(function(index, value){
          $(this).on('click', function() {
            chooseVintage(this)
          })
        })
      });


      function changeVintagesActiveValues() {
        let activeValues = [];

        $('.vintage-list input').each(function(){
          if($(this).is(':checked')) {
            activeValues.push($(this).val())
          }
        })

        if(activeValues.length == 1) {
          $('#fromList .custom-select__name').text(activeValues[0])
          $('#toList .custom-select__name').text(activeValues[0])
        } else if (activeValues.length > 2) {
          activeValues.sort();
          $('#fromList .custom-select__name').text(activeValues[0])
          $('#toList .custom-select__name').text(activeValues[activeValues.length - 1])
        }
      }

      changeVintagesActiveValues();

      function chooseVintage(elem) {

        let elemValue = $(elem).data('value'),
            elemType = $(elem).data('type');
        $(elem).closest('.custom-select').find('.custom-select__name').text(elemValue);
  
        if(elemType == 'from') {
          let toElem = Number($('#toList .custom-select__name').text());
          activateFilter(elemValue, toElem)
        } else if (elemType == 'to'){
          let fromElem = Number($('#fromList .custom-select__name').text());
          activateFilter(fromElem, elemValue)
        }
      }
  
      function activateFilter(from, to) {
        if(to > from || to == from) {
          filterEvents(from, to );
        } else {
          filterEvents(to, from);
        }
      }
  
      function getVintagesValues() {
        let items = [];
  
        $('.vintage-list li input').each(function(index, value){
          items.push($(this).val());
        })
  
        return items;
      }
  
      function filterEvents(from, to) {
        let vintages = getVintagesValues();
  
        for (i of vintages) {
          let field = $(`.vintage-list input[value=${i}]`)[0];
          
          if(i >= from && i <= to) {
            if($(field).is(':checked')) $(field).click();
            $(field).click();
          } else {  
            if($(field).is(':checked')) $(field).click();
          }
        }
      }
    }

    function closeFilterItems(event) {
      $('.js-filter').each(function() {
        if ( !$(this).is(event.target)
          && $(this).has(event.target).length === 0 ) {
          $( this ).removeAttr( "open" );
        }
      });
    }

    function closeVintageSelect(event) {
      let activeSelect = $('.custom-select.m-sl-open'),
          selectLabel = $(activeSelect).find('.custom-select__name'),
          selectList = $(activeSelect).find('.custom-select__list');
      
      if ( !$(selectLabel).is(event.target)
      && $(selectLabel).has(event.target).length === 0 ) {
        $(activeSelect).removeClass('m-sl-open');
        $(selectList).hide();
      }
    }

    function onBodyClick(event) {
      if ($('.facets-container').length > 0) closeFilterItems(event);
      if($('.vintage-filter-box').length > 0) closeVintageSelect(event);  
    }

    document.body.addEventListener('click', onBodyClick);
  })

})(jQuery);