(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.accordion__heading', function() {
            if($(this).parent().hasClass('m-block-active')) {
                $(this).parent().removeClass('m-block-active');
            } else {
                $(this).parent().addClass('m-block-active');
                if ($(window).width() > 990) {
                    $('html, body').animate({
                        scrollTop: $(this).parent().offset().top - 80
                    }, 1000);
                } else {
                    $('html, body').animate({
                        scrollTop: $(this).parent().offset().top - 60
                    }, 1000);
                }
            }

        });

        if($('.about-table').length) {
            let indexTable = 4;
            function openTableBlocks() {
                $('.about-table__block').each(function(index,value){
                    if(index + 1 > indexTable) {
                        $(this).addClass('m-table-block-hide');
                    } else {
                        $(this).removeClass('m-table-block-hide');
                    }
                })
                if($('.m-table-block-hide').length === 0) {
                    $('.about-table__btn').addClass('m-hide');
                }
            }
            openTableBlocks();

            $(document).on('click', '.about-table__btn', function() {
                indexTable = indexTable + 4
                openTableBlocks();
            })
        }

        if($('.agenda').length) {
            let indexTable = 4;
            function openTableBlocks() {
                $('.agenda__item').each(function(index,value){
                    if(index + 1 > indexTable) {
                        $(this).addClass('m-agenda-item-hide');
                    } else {
                        $(this).removeClass('m-agenda-item-hide');
                    }
                })
                if($('.m-agenda-item-hide').length === 0) {
                    $('.agenda__btn').addClass('m-hide');
                }
            }
            openTableBlocks();

            $(document).on('click', '.agenda__btn', function() {
                indexTable = indexTable + 4
                openTableBlocks();
            })
        }

        if($('.hm-banner').length) {
            let widthWindow = $(window).width();
            let trigger = null;
            let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

            if(scrollTop >= trigger) {
                $('.header__heading-link').removeClass('m-logo-hide');
            } else {
                $('.header__heading-link').addClass('m-logo-hide');
            }

            $(window).scroll(function () {
                let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

                if(widthWindow > 990) {
                    trigger = $('.hm-banner__video').offset().top
                } else {
                    trigger = $('.hm-banner__slider').offset().top
                }

                if(scrollTop >= trigger) {
                    $('.header__heading-link').addClass('m-logo-show');
                } else {
                    $('.header__heading-link').removeClass('m-logo-show');
                }
            });

            $(document).on('click', 'header-drawer', function() {
                $('.header__heading-link').addClass('m-logo-show');
            });
        } else {
            $('.header__heading-link').addClass('m-logo-show');
        }

        if ($('.pr-custom__variant').length) {
            $('.js-sl-custom option').each(function() {
                if ($(this).attr('selected') === 'selected') {
                    let dataIndex = $(this).attr('data-index');
                    let dataValue = $(this).attr('value');

                    $(`.select-custom__item[data-index='${dataIndex}'][data-value='${dataValue}']`).addClass('m-item-active');
                    $(`.select-custom__item[data-index='${dataIndex}'][data-value='${dataValue}`).closest('.select-custom').find('.select-custom__name').text(dataValue);
                }
            });

            $(document).on('click', '.select-custom__name', function() {
                $(this).closest('.select-custom').toggleClass('m-sl-open');
            });

            $(document).on('click', '.select-custom__item', function() {
                let option1 = '';
                let option2 = '';
                let variant = '';
                let dataIndex = $(this).attr('data-index');
                let dataValue = $(this).attr('data-value');
                let urlProd = $('.pr-custom__variant').attr('data-url');
                let price = $('.pr-custom .price').parent();
                let productFormBtn = $('.pr-custom .product-form__buttons');
                let productFormSubmitWrapper = $('.pr-custom .product-form__wrapper');

                $(this).closest('.select-custom__list').find('.m-item-active').removeClass('m-item-active');


                $(`.js-sl-custom [value='${dataValue}']`).closest('.js-sl-custom').find('option:selected').prop('selected',false);
                $(`.js-sl-custom [value='${dataValue}']`).closest('.js-sl-custom').find('option:selected').removeAttr('selected');
                $(`.js-sl-custom [value='${dataValue}']`).prop('selected',true);
                $(`.js-sl-custom [value='${dataValue}']`).attr("selected", "selected");
                $(`.js-sl-custom [value='${dataValue}']`).closest('.js-sl-custom').trigger('change');
                $(`.js-sl-custom [value='${dataValue}']`).closest('.js-sl-custom').click();

                $(this).addClass('m-item-active');
                let nameValue = $(this).attr('data-value');
                $(this).closest('.select-custom').find('.select-custom__name').text(nameValue);


                option1 = $('.select-custom[data-option="data-option1"] .m-item-active').attr('data-value');
                option2 = $('.select-custom[data-option="data-option2"] .m-item-active').attr('data-value');

                variant = $(`.pr-custom__variants li[data-option1='${option1}'][data-option2='${option2}']`).attr('value');

                $('.product-form input[name="id"]').val(variant);
                $('.product-form .form').trigger('change');

                window.history.replaceState({ }, '', `${urlProd}?variant=${variant}`);

                fetch(`${$(location).attr("href")}&section_id=${$('.pr-custom__variants').attr('section-id')}`)
                .then((response) => response.text())
                .then((responseText) => {
                    const html = $($.parseHTML(responseText))
                    html.each(function(){
                        let newPrice = $(this).find('.pr-custom').find('.price');
                        let newProductFormBtn = $(this).find('.pr-custom').find('.product-form__submit');
                        let newWishlist = $(this).find('.pr-custom').find('[button-wishlist]');
                        price.html(newPrice);
                        productFormBtn.find('.product-form__submit').remove();
                        if(newProductFormBtn.attr('disabled')) {
                            productFormBtn.find('.shopify-payment-button__button').prop("disabled",true);
                        } else {
                            productFormBtn.find('.shopify-payment-button__button').prop("disabled",false);
                        }
                        $(productFormSubmitWrapper).append(newProductFormBtn);
                        $(productFormSubmitWrapper).find('.card-prod__wishlist').html(newWishlist);
                        reInitWishlist($(newWishlist).data('variant-id'), newWishlist);
                    });
                });

                $(this).closest('.m-sl-open').removeClass('m-sl-open');
            });

        }

        function reInitWishlist(variant, button) {
            const wishlist = localStorage.getItem(LOCAL_STORAGE_WISHLIST_KEY) || false;

            if (wishlist) {
                let array = JSON.parse(wishlist);
                if(array.filter(item => item.variant === `${variant}`).length > 0) {
                    $(button).addClass('active');
                } else {
                    $(button).removeClass('active');
                }
            }

            const event = new CustomEvent('shopify-wishlist:button-changed');
            document.dispatchEvent(event);
        }

        if($('#FacetFiltersForm').length) {
            function activeFilterItem() {
                $('.filter-item').each(function() {
                    if($(this).find('.facets__reset').length) {
                        $(this).addClass('m-active');
                    } else {
                        $(this).removeClass('m-active');
                    }
                });
            }

            activeFilterItem();

            $('#FacetFiltersForm').change(function() {
                setTimeout(function() {
                    activeFilterItem();
                },2000)
            });
        }
    });
})(jQuery);