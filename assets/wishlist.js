const LOCAL_STORAGE_WISHLIST_KEY = 'shopify-wishlist';
const LOCAL_STORAGE_DELIMITER = ',';
const BUTTON_ACTIVE_CLASS = 'active';
const GRID_LOADED_CLASS = 'loaded';

const selectors = {
  button: '[button-wishlist]',
  grid: '[grid-wishlist]',
  productCard: '.product-card',
};

document.addEventListener('DOMContentLoaded', () => {
  initButtons();
  initGrid();
});

document.addEventListener('shopify-wishlist:updated', (event) => {
  console.log('[Shopify Wishlist] Wishlist Updated ✅', event.detail.wishlist);
  initGrid();
});

document.addEventListener('shopify-wishlist:init-product-grid', (event) => {
  console.log('[Shopify Wishlist] Wishlist Product List Loaded ✅', event.detail.wishlist);
  checkProducts();
});

document.addEventListener('shopify-wishlist:init-buttons', (event) => {
  console.log('[Shopify Wishlist] Wishlist Buttons Loaded ✅', event.detail.wishlist);
});

const fetchProductCardHTML = (item) => {
  const handle = item.handle;
  const variant = item.variant;
  const lang = document.getElementsByTagName('html')[0].getAttribute('lang');
  const productTileTemplateUrl = (lang !== 'en' ? `/${lang}/products/${handle}?view=card&variant=${variant}` : `/products/${handle}?view=card&variant=${variant}`);
  return fetch(productTileTemplateUrl)
  .then((res) => res.text())
  .then((res) => {
    const text = res;
    const parser = new DOMParser();
    const htmlDocument = parser.parseFromString(text, 'text/html');
    const productCard = htmlDocument.documentElement.querySelector(selectors.productCard);
    return productCard.outerHTML;
  })
  .catch((err) => console.error(`[Shopify Wishlist] Failed to load content for handle: ${handle}`, err));
};

const setupGrid = async (grid) => {
  const wishlist = getWishlist();
  const requests = wishlist.map(fetchProductCardHTML);
  const responses = await Promise.all(requests);
  const wishlistProductCards = responses.join('');
  grid.innerHTML = wishlistProductCards;
  if(!wishlistProductCards) {
    let emptyField = document.querySelector('.wishlist-empty');
    emptyField.classList.remove('hidden')
  }
  grid.classList.add(GRID_LOADED_CLASS);
  
  initButtons();
  initCartBtn();
  initLoadMore();
  initAddAllButton();

  const event = new CustomEvent('shopify-wishlist:init-product-grid', {
    detail: { wishlist: wishlist }
  });
  document.dispatchEvent(event);
};

const initLoadMore = () => {
  let products = document.querySelectorAll('.wishlist__grid .product-card'),
      btn = document.querySelector('.wishlist-load-more');

  if(products.length > 4) {
    btn.classList.remove('hidden');
    btn.addEventListener('click', function(){
      products.forEach((item) => {
        item.style.display = 'block';
        btn.classList.add('hidden');
      })
    })
  }
}

const checkProducts = () => {
  let products = document.querySelectorAll('.wishlist__grid .product-card'),
      btn = document.querySelector('.wishlist-load-more');
  if(products.length > 4) {
    btn.classList.remove('hidden');
  } else {
    btn.classList.add('hidden');
  }
}

const initCartBtn = () => {
  let addToCart = document.querySelectorAll('.add_to_cart'),
      container = document.querySelector('.js-wishlist');
  addToCart.forEach((item) => {
    item.addEventListener('click', (event) => {
      container.classList.add('loading');
      let addData = {
        'id': event.target.getAttribute('data-id'),
        'quantity':1
      };
      $.ajax({
        type: 'POST',
        url: '/cart/add.js',
        dataType: 'json',
        data: addData,
        success: function(res){
            let handle = event.target.getAttribute('data-handle');
            renderCart()
            updateWishlist(handle, event.target.getAttribute('data-id'));
        },
        error: function(error){
          event.target.nextSibling.textContent = error.responseJSON.description;
          event.target.nextSibling.toggleAttribute('hidden');
        }
      });
      setTimeout(function(){
        container.classList.remove('loading');
      }, 300)
    });
  })
};

const renderCart = () => {
  $.ajax({
    type: 'GET',
    url: '/cart',
    success: function(res){
      const parser = new DOMParser();
      const doc = parser.parseFromString(res, "text/html");
      const formSelector = doc.querySelector("#cart-icon-bubble");
      document.getElementById("cart-icon-bubble").replaceWith(formSelector);
    },
    error: function(error){
    }
  });
}

const initAddAllButton =  async () => {
  let productsList = document.querySelectorAll('.wishlist__grid .product-card .add_to_cart'),
      addAllBtn = document.getElementById('add-all-button'),
      formData = [];

  productsList.forEach((item) => {
    formData.push(item.getAttribute('data-id'))
  })

  if(productsList.length > 0) {
    addAllBtn.removeAttribute('disabled');
    addAllBtn.addEventListener('click', function(){
      addAllProducts(formData)
    })
  }
};

const addAllProducts = async (array) => {
  let container = document.querySelector('.js-wishlist'),
      variants = [],
      newArray = array.join().split(',');

  container.classList.add('loading');
  Shopify.queue = [];

  for (var i = 0; i < newArray.length; i++) {
    product = newArray[i]
    Shopify.queue.push({
      variantId: product,
    });
  }

  Shopify.moveAlong = function() {
    if (Shopify.queue.length) {

      let request = Shopify.queue.shift(),
          data = 'id='+ request.variantId + '&quantity=1',
          elem = document.querySelector(`[data-id="${request.variantId}"]`);

      $.ajax({
        type: 'POST',
        url: '/cart/add.js',
        dataType: 'json',
        data: data,
        success: function(res){
          let handle = elem.getAttribute('data-handle');
          variants.push(request.variantId);

          renderCart();
          
          if(Shopify.queue.length == 0) {
            container.classList.remove('loading');
            document.querySelector('.wishlist-result').classList.remove('hidden');
            removeWishlistItems(variants)
          }

          checkProducts();
          Shopify.moveAlong();
        },
        error: function(error){
          let errorWrapper = elem.closest('.product-card').querySelector('.product-form__error-message-wrapper');

          errorWrapper.textContent = error.responseJSON.description;
          errorWrapper.removeAttribute('hidden');
          if (Shopify.queue.length){
            Shopify.moveAlong()
          }

          container.classList.remove('loading');
        }
      });
    }
  };
  Shopify.moveAlong();
}

const setupButtons = (buttons) => {
  buttons.forEach((button) => {
    const productHandle = button.dataset.productHandle || false;
    const variantId = button.dataset.variantId || false;
    if (!productHandle) return;
    if (wishlistContains(variantId)) button.classList.add(BUTTON_ACTIVE_CLASS);
    if(wishlistContainsProduct(productHandle) && !variantId) button.classList.add(BUTTON_ACTIVE_CLASS);
    
    if(variantId) {
      button.addEventListener('click', () => {
        updateWishlist(productHandle, variantId);
        button.classList.toggle(BUTTON_ACTIVE_CLASS);
      });
    }
  });
};

const initGrid = () => {
  const grid = document.querySelector(selectors.grid) || false;
  if (grid) setupGrid(grid);
};

const initButtons = () => {
  const buttons = document.querySelectorAll(selectors.button) || [];
  if (buttons.length) setupButtons(buttons);
  else return;
  const event = new CustomEvent('shopify-wishlist:init-buttons', {
    detail: { wishlist: getWishlist() }
  });
  document.dispatchEvent(event);
};

const getWishlist = () => {
  const wishlist = localStorage.getItem(LOCAL_STORAGE_WISHLIST_KEY) || false;
  if (wishlist) return JSON.parse(wishlist);
  return [];
};

const setWishlist = (array) => {
  const wishlist = array;
  const addAllBtn = document.getElementById('add-all-button');
  if (array.length) localStorage.setItem(LOCAL_STORAGE_WISHLIST_KEY, JSON.stringify(array));
  else localStorage.removeItem(LOCAL_STORAGE_WISHLIST_KEY);

  const event = new CustomEvent('shopify-wishlist:updated', {
    detail: { wishlist: array }
  });
  document.dispatchEvent(event);

  if(wishlist.length == 0 && addAllBtn) {
    addAllBtn.setAttribute('disabled', 'disabled');
  }

  return wishlist;
};

const updateWishlist = (handle, variant) => {
  const wishlist = getWishlist();
  const item = {
    handle: handle,
    variant: variant
  };
  if (wishlist.length > 0){
    let indexInWishlist = wishlist.findIndex(element => {
      return element.variant === variant;
    });
    if(indexInWishlist === -1) {
      wishlist.push(item);
    } else {
      wishlist.splice(indexInWishlist, 1);
    }
  } else {
    wishlist.push(item);
  }

  return setWishlist(wishlist);
};

const removeWishlistItems = (array) => {
  const wishlist = getWishlist();
  array.forEach((variant) => {
    let indexInWishlist = wishlist.findIndex(element => {
      return element.variant === variant;
    });
    wishlist.splice(indexInWishlist, 1);
  })
  
  return setWishlist(wishlist);
};

const wishlistContains = (variant) => {
  const wishlist = getWishlist();
  return wishlist.filter(item => item.variant === variant).length > 0;
};

const wishlistContainsProduct = (handle) => {
  const wishlist = getWishlist();
  return wishlist.filter(item => item.handle === handle).length > 0;
};

const resetWishlist = () => {
  return setWishlist([]);
};

document.addEventListener('shopify-wishlist:button-changed', () => {
  let button = document.querySelector('.pr-custom [button-wishlist]');
  const productHandle = button.dataset.productHandle || false;
  const variantId = button.dataset.variantId || false;
  
  if(productHandle && variantId) {
    button.addEventListener('click', () => {
      updateWishlist(productHandle, variantId);
      button.classList.toggle(BUTTON_ACTIVE_CLASS);
    });
  }
});